#!/bin/bash
set -e

if [ "$1" = 'start' ]; then
  echo "Coucou, je demarre !"
  exec npm start
elif [ "$1" = 'debug' ]; then
  DEBUG=true exec npm start
fi

exec "$@"